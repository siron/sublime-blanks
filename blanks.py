import sublime, sublime_plugin

class TrimWhitespaceCommand(sublime_plugin.TextCommand):
    def run(self, edit, **args):
        if args['whichPart'] == "leading":
            whitespaces = self.view.find_all("^[\t ]+")
        elif args['whichPart'] == "trailing":
            whitespaces = self.view.find_all("[\t ]+$")
        elif args['whichPart'] == "both":
            whitespaces = self.view.find_all("(^[\t ]+|[\t ]+$)")

        whitespaces.reverse()
        for r in whitespaces:
            self.view.erase(edit, r)

class RemoveEmptyLineCommand(sublime_plugin.TextCommand):
    def run(self, edit, **args):
        if args['includeBlank']:
            empty_lines = self.view.find_all("^[\t ]*$(\r\n|\r|\n)")
        elif not args['includeBlank']:
            empty_lines = self.view.find_all("^$(\r\n|\r|\n)")

        empty_lines.reverse()
        for r in empty_lines:
            self.view.erase(edit, r)

        # remove the last line if it's an empty line
        if args['includeBlank']:
            empty_lines = self.view.find_all("(\r\n|\r|\n)^[\t ]*$")
        elif not args['includeBlank']:
            empty_lines = self.view.find_all("(\r\n|\r|\n)^$")

        empty_lines.reverse()
        for r in empty_lines:
            self.view.erase(edit, r)

