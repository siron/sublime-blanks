# Description
This plugin helps you perform clean up operations on your source files to get rid of those useless whitespaces that you might not realize adding while working on your codes.
 

# Operations Supported
* Trim leading whitespace
* Trim trailing whitespace
* Trim leading and trailing whitespace
* Remove blank lines
* Remove blank lines (containing blank characters)
 

# Usage
All plugin commands can be accessed from the Edit menu under the "Blanks" submenu and also from Command Palette (Ctrl+Shift+P)